from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from django.http import FileResponse, Http404
from .models import Post

# Create your views here.
class BlogListView(ListView):
    model = Post
    template_name = 'home.html'
    queryset = Post.objects.order_by('-timestamp')[:5]

class BlogDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'
    
class UnderConstructionView(TemplateView):
    template_name = 'under_construction.html'
    
class ResumeView(TemplateView):
    template_name = 'resume.html'