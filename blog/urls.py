from django.urls import path
from . import views

urlpatterns = [
    path('', views.BlogListView.as_view(), name='home'),
    path('post/<int:pk>/', views.BlogDetailView.as_view(), name='post_detail'),
    path('under_construction/', views.UnderConstructionView.as_view(), name='under_construction'),
    path('resume/', views.ResumeView.as_view(), name='resume'),
]