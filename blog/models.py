from django.db import models
from tagging.fields import TagField
from django.urls import reverse
from django.conf import settings

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    updated = models.DateField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    body = models.TextField()
    tags = TagField()
    
    def __str__(self):
        return self.title
        
    def get_absolute_url(self):
        return reverse('home')
        
    def is_previous(self):
        try:
            self.get_previous_by_timestamp()
            return True
        except:
            return False
        
    def get_next(self):
        try:
            self.get_next_by_timestamp()
            return True
        except:
            return False
